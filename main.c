#include "shaders.h"
#include "texture.h"

#define VK_USE_PLATFORM_XLIB_KHR
#include <vulkan/vulkan.h>

#include <SDL.h>
#include <SDL_syswm.h>

#include <inttypes.h>
#include <stdlib.h>

static const float DATA[24] = {
	-1.0, -1.0,  1.0,
	 1.0, -1.0,  1.0,
	 1.0,  1.0,  1.0,
	-1.0,  1.0,  1.0,
	-1.0, -1.0, -1.0,
	 1.0, -1.0, -1.0,
	 1.0,  1.0, -1.0,
	-1.0,  1.0, -1.0,
};

static const uint16_t INDICES[36] = {
	0, 1, 2,
	2, 3, 0,
	1, 5, 6,
	6, 2, 1,
	7, 6, 5,
	5, 4, 7,
	4, 0, 3,
	3, 7, 4,
	4, 5, 1,
	1, 0, 4,
	3, 2, 6,
	6, 7, 3,
};

static const float MVP[48] = {
	 0.766,  0.0,  0.642, 0.0,
	   0.0,  1.0,  0.0,   0.0,
	-0.642,  0.0,  0.766, 0.0,
	   0.0,  0.0, -4.5,   1.0,

	1.000000,  0.000000,  0.000000, 0.000000,
	0.000000,  0.894427,  0.447214, 0.000000,
	0.000000, -0.447214,  0.894427, 0.000000,
	0.000000, -1.788854, -0.894427, 1.000000,

	1.810660,  0.000000,  0.000000,  0.000000,
	0.000000, -2.414213,  0.000000,  0.000000,
	0.000000,  0.000000, -1.020202, -1.000000,
	0.000000,  0.000000, -0.202020,  0.000000,
};

static const char* const LAYERS[] = { "VK_LAYER_LUNARG_standard_validation" };
static const char* const INSTANCE_EXTENSIONS[] = {
	VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_XLIB_SURFACE_EXTENSION_NAME
};

static const char* const DEVICE_EXTENSIONS[] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

int main(void)
{
	int retval = EXIT_FAILURE;

	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		goto out1;
	}

	VkApplicationInfo app_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pNext = NULL,
		.pApplicationName = "Hello world",
		.applicationVersion = 0,
		.pEngineName = "Hello world",
		.engineVersion = 0,
		.apiVersion = VK_API_VERSION_1_0,
	};

	VkInstanceCreateInfo create_instance_info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.pApplicationInfo = &app_info,
		.enabledLayerCount = 1,
		.ppEnabledLayerNames = LAYERS,
		.enabledExtensionCount = 2,
		.ppEnabledExtensionNames = INSTANCE_EXTENSIONS,
	};

	VkInstance instance;
	VkResult err = vkCreateInstance(&create_instance_info, NULL, &instance);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create Vulkan instance");
		goto out2;
	}

	uint32_t num_devices;
	err = vkEnumeratePhysicalDevices(instance, &num_devices, NULL);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not enumerate Vulkan physical devices");
		goto out3;
	}
	if (num_devices == 0) {
		SDL_Log("No physical devices that support Vulkan were found");
		goto out3;
	}

	VkPhysicalDevice* devices = malloc(num_devices * sizeof(devices[0]));
	if (devices == NULL) {
		SDL_Log("Could not allocate memory for Vulkan physical devices");
		goto out3;
	}

	err = vkEnumeratePhysicalDevices(instance, &num_devices, devices);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not enumerate Vulkan physical devices");
		goto out4;
	}

	for (uint32_t i = 0; i < num_devices; i++) {
		uint32_t num_queue_families;
		vkGetPhysicalDeviceQueueFamilyProperties(devices[i], &num_queue_families, NULL);

		VkQueueFamilyProperties* queue_families = malloc(num_queue_families * sizeof(queue_families[0]));
		vkGetPhysicalDeviceQueueFamilyProperties(devices[i], &num_queue_families, queue_families);

		free(queue_families);
	}

	VkPhysicalDeviceMemoryProperties mem_properties;
	for (uint32_t i = 0; i < num_devices; i++) {
		vkGetPhysicalDeviceMemoryProperties(devices[i], &mem_properties);
	}

	VkPhysicalDeviceProperties device_limits;
	vkGetPhysicalDeviceProperties(devices[0], &device_limits);

	VkDeviceQueueCreateInfo device_queue_create = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueFamilyIndex = 0,
		.queueCount = 1,
		.pQueuePriorities = (float[]){1.0},
	};

	VkPhysicalDeviceFeatures features = {
		.samplerAnisotropy = VK_TRUE,
	};

	VkDeviceCreateInfo device_create = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &device_queue_create,
		.enabledLayerCount = 1,
		.ppEnabledLayerNames = LAYERS,
		.enabledExtensionCount = 1,
		.ppEnabledExtensionNames = DEVICE_EXTENSIONS,
		.pEnabledFeatures = &features,
	};

	VkDevice device;
	err = vkCreateDevice(devices[0], &device_create, NULL, &device);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create Vulkan device");
		goto out4;
	}

	VkQueue queue;
	vkGetDeviceQueue(device, 0, 0, &queue);

	SDL_Window* window = SDL_CreateWindow("Hello Vulkan!",
										  SDL_WINDOWPOS_CENTERED,
										  SDL_WINDOWPOS_CENTERED,
										  1024, 768,
										  SDL_WINDOW_SHOWN);
	if (window == NULL) {
		SDL_Log("Could not create window: %s", SDL_GetError());
		goto out5;
	}

	SDL_SysWMinfo win_info;
	SDL_VERSION(&win_info.version);
	if (!SDL_GetWindowWMInfo(window, &win_info)) {
		SDL_Log("Could not retrieve window info: %s", SDL_GetError());
		goto out6;
	}

	VkSurfaceKHR surface;
	VkXlibSurfaceCreateInfoKHR surface_create_info = {
		.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
		.pNext = NULL,
		.flags = 0,
		.dpy = win_info.info.x11.display,
		.window = win_info.info.x11.window,
	};

	err = vkCreateXlibSurfaceKHR(instance, &surface_create_info, NULL, &surface);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create Vulkan Xlib surface");
		goto out6;
	}

	VkBool32 present_support;
	vkGetPhysicalDeviceSurfaceSupportKHR(devices[0], 0, surface, &present_support);
	if (!present_support) {
		SDL_Log("Vulkan physical device doesn't support presentation");
		goto out7;
	}

	VkSurfaceCapabilitiesKHR surface_caps;
	err = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(devices[0], surface, &surface_caps);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not get Vulkan surface capabilities");
		goto out7;
	}

	uint32_t num_surface_formats;
	err = vkGetPhysicalDeviceSurfaceFormatsKHR(devices[0], surface, &num_surface_formats, NULL);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not get Vulkan surface formats");
		goto out7;
	}

	VkSurfaceFormatKHR* surface_formats = malloc(num_surface_formats * sizeof(surface_formats[0]));
	err = vkGetPhysicalDeviceSurfaceFormatsKHR(devices[0], surface, &num_surface_formats, surface_formats);
	free(surface_formats);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not get Vulkan surface formats");
		goto out7;
	}

	VkSwapchainKHR swapchain;
	VkSwapchainCreateInfoKHR swapchain_info = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.pNext = NULL,
		.flags = 0,
		.surface = surface,
		.minImageCount = surface_caps.minImageCount,
		.imageFormat = VK_FORMAT_B8G8R8A8_SRGB,
		.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
		.imageExtent = surface_caps.currentExtent,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
		.preTransform = surface_caps.currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = VK_PRESENT_MODE_FIFO_KHR,
		.clipped = VK_TRUE,
		.oldSwapchain = VK_NULL_HANDLE,
	};
	err = vkCreateSwapchainKHR(device, &swapchain_info, NULL, &swapchain);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create Vulkan swapchain");
		goto out7;
	}

	uint32_t image_count;
	err = vkGetSwapchainImagesKHR(device, swapchain, &image_count, NULL);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not get number of swapchain images");
		goto out8;
	}

	VkImage* images = malloc(image_count * sizeof(images[0]));
	vkGetSwapchainImagesKHR(device, swapchain, &image_count, images);

	VkCommandPool cmd_pool;
	VkCommandPoolCreateInfo pool_create_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueFamilyIndex = 0
	};
	err = vkCreateCommandPool(device, &pool_create_info, NULL, &cmd_pool);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create command pool");
		goto out9;
	}

	VkCommandBuffer cmd_buffers[3];
	VkCommandBufferAllocateInfo cmd_buffer_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.pNext = NULL,
		.commandPool = cmd_pool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = 3,
	};
	err = vkAllocateCommandBuffers(device, &cmd_buffer_info, cmd_buffers);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create command buffers");
		goto out10;
	}

	VkSemaphore semaphore_img_available;
	VkSemaphore semaphore_rendering_done;
	VkSemaphoreCreateInfo semaphore_info = {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
	};
	err = vkCreateSemaphore(device, &semaphore_info, NULL, &semaphore_img_available);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create semaphore");
		goto out10;
	}
	err = vkCreateSemaphore(device, &semaphore_info, NULL, &semaphore_rendering_done);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create semaphore");
		goto out11;
	}

	VkRenderPass render_pass;
	VkAttachmentDescription attachments[3] = {
		[0] = {
			.flags = 0,
			.format = VK_FORMAT_B8G8R8A8_SRGB,
			.samples = VK_SAMPLE_COUNT_8_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
		},
		[1] = {
			.flags = 0,
			.format = VK_FORMAT_B8G8R8A8_SRGB,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
		},
		[2] = {
			.flags = 0,
			.format = VK_FORMAT_D24_UNORM_S8_UINT,
			.samples = VK_SAMPLE_COUNT_8_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
		},
	};
	VkSubpassDescription subpass = {
		.flags = 0,
		.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
		.inputAttachmentCount = 0,
		.pInputAttachments = NULL,
		.colorAttachmentCount = 1,
		.pColorAttachments = (VkAttachmentReference[]){
			[0] = {
				.attachment = 0,
				.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			},
		},
		.pResolveAttachments = (VkAttachmentReference[]) {
			[0] = {
				.attachment = 1,
				.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			},
		},
		.pDepthStencilAttachment = (VkAttachmentReference[]) {
			[0] = {
				.attachment = 2,
				.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			},
		},
		.preserveAttachmentCount = 0,
		.pPreserveAttachments = NULL,
	};
	VkSubpassDependency dependency = {
		.srcSubpass = VK_SUBPASS_EXTERNAL,
		.dstSubpass = 0,
		.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		.srcAccessMask = 0,
		.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT
			| VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
		.dependencyFlags = 0,
	};
	VkRenderPassCreateInfo render_pass_info = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.attachmentCount = 3,
		.pAttachments = attachments,
		.subpassCount = 1,
		.pSubpasses = &subpass,
		.dependencyCount = 1,
		.pDependencies = &dependency,
	};
	err = vkCreateRenderPass(device, &render_pass_info, NULL, &render_pass);

	if (err != VK_SUCCESS) {
		SDL_Log("Could not create render pass");
		goto out12;
	}

	VkImage msaa_buffer;
	VkImageCreateInfo msaa_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.imageType = VK_IMAGE_TYPE_2D,
		.format = VK_FORMAT_B8G8R8A8_SRGB,
		.extent = {.width = 1024, .height = 768, .depth = 1},
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = VK_SAMPLE_COUNT_8_BIT,
		.tiling = VK_IMAGE_TILING_OPTIMAL,
		.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	};

	err = vkCreateImage(device, &msaa_info, NULL, &msaa_buffer);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create multisample buffer");
		goto out12a;
	}

	VkImage depth_buffer;
	VkImageCreateInfo depth_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.imageType = VK_IMAGE_TYPE_2D,
		.format = VK_FORMAT_D24_UNORM_S8_UINT,
		.extent = {.width = 1024, .height = 768, .depth = 1},
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = VK_SAMPLE_COUNT_8_BIT,
		.tiling = VK_IMAGE_TILING_OPTIMAL,
		.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	};
	err = vkCreateImage(device, &depth_info, NULL, &depth_buffer);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create depth_buffer buffer");
		goto out12b;
	}

	VkImage texture;
	VkImageCreateInfo texture_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		.pNext = NULL,
		.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT,
		.imageType = VK_IMAGE_TYPE_2D,
		.format = VK_FORMAT_B8G8R8A8_SRGB,
		.extent = {.width = 512, .height = 512, .depth = 1},
		.mipLevels = 10,
		.arrayLayers = 6,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.tiling = VK_IMAGE_TILING_OPTIMAL,
		.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT
			| VK_IMAGE_USAGE_TRANSFER_DST_BIT
			| VK_IMAGE_USAGE_SAMPLED_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	};
	err = vkCreateImage(device, &texture_info, NULL, &texture);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create texture");
		goto out12c;
	}

	VkMemoryRequirements msaa_reqs;
	vkGetImageMemoryRequirements(device, msaa_buffer, &msaa_reqs);
	VkMemoryRequirements depth_reqs;
	vkGetImageMemoryRequirements(device, depth_buffer, &depth_reqs);
	VkMemoryRequirements texture_reqs;
	vkGetImageMemoryRequirements(device, texture, &texture_reqs);

	VkDeviceMemory msaa_mem;

	VkDeviceSize msaa_start = 0;
	VkDeviceSize msaa_end = msaa_reqs.size;

	VkDeviceSize depth_buffer_start = (msaa_end + depth_reqs.alignment - 1) & (-depth_reqs.alignment);
	VkDeviceSize depth_buffer_end = depth_buffer_start + depth_reqs.size;

	VkDeviceSize texture_start = (depth_buffer_end + texture_reqs.alignment - 1) & (-texture_reqs.alignment);
	VkDeviceSize texture_end = texture_start + texture_reqs.size;

	VkDeviceSize size = texture_end;

	VkMemoryAllocateInfo msaa_alloc_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.pNext = NULL,
		.allocationSize = size,
		.memoryTypeIndex = 0,
	};
	err = vkAllocateMemory(device, &msaa_alloc_info, NULL, &msaa_mem);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create multisample buffer memory");
		goto out12d;
	}
	vkBindImageMemory(device, msaa_buffer, msaa_mem, msaa_start);
	vkBindImageMemory(device, depth_buffer, msaa_mem, depth_buffer_start);
	vkBindImageMemory(device, texture, msaa_mem, texture_start);

	VkImageView msaa_view;
	VkImageViewCreateInfo msaa_view_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.image = msaa_buffer,
		.viewType = VK_IMAGE_VIEW_TYPE_2D,
		.format = VK_FORMAT_B8G8R8A8_SRGB,
		.components = {
			.r = VK_COMPONENT_SWIZZLE_IDENTITY,
			.g = VK_COMPONENT_SWIZZLE_IDENTITY,
			.b = VK_COMPONENT_SWIZZLE_IDENTITY,
			.a = VK_COMPONENT_SWIZZLE_IDENTITY,
		},
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.baseMipLevel = 0,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1,
		},
	};
	err = vkCreateImageView(device, &msaa_view_info, NULL, &msaa_view);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create render pass");
		goto out12e;
	}

	VkImageView depth_view;
	VkImageViewCreateInfo depth_view_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.image = depth_buffer,
		.viewType = VK_IMAGE_VIEW_TYPE_2D,
		.format = VK_FORMAT_D24_UNORM_S8_UINT,
		.components = {
			.r = VK_COMPONENT_SWIZZLE_IDENTITY,
			.g = VK_COMPONENT_SWIZZLE_IDENTITY,
			.b = VK_COMPONENT_SWIZZLE_IDENTITY,
			.a = VK_COMPONENT_SWIZZLE_IDENTITY,
		},
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT,
			.baseMipLevel = 0,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1,
		},
	};
	err = vkCreateImageView(device, &depth_view_info, NULL, &depth_view);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create render pass");
		goto out12f;
	}

	VkImageView texture_view;
	VkImageViewCreateInfo texture_view_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.image = texture,
		.viewType = VK_IMAGE_VIEW_TYPE_CUBE,
		.format = VK_FORMAT_B8G8R8A8_SRGB,
		.components = {
			.r = VK_COMPONENT_SWIZZLE_IDENTITY,
			.g = VK_COMPONENT_SWIZZLE_IDENTITY,
			.b = VK_COMPONENT_SWIZZLE_IDENTITY,
			.a = VK_COMPONENT_SWIZZLE_IDENTITY,
		},
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.baseMipLevel = 0,
			.levelCount = 10,
			.baseArrayLayer = 0,
			.layerCount = 6,
		},
	};
	err = vkCreateImageView(device, &texture_view_info, NULL, &texture_view);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create render pass");
		goto out12g;
	}

	uint32_t num_image_views = 0;
	VkImageView* image_views = malloc(image_count * sizeof(image_views[0]));
	for (uint32_t i = 0; i < image_count; i++) {
		VkImageViewCreateInfo info = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.pNext = NULL,
			.flags = 0,
			.image = images[i],
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = VK_FORMAT_B8G8R8A8_SRGB,
			.components = {
				.r = VK_COMPONENT_SWIZZLE_IDENTITY,
				.g = VK_COMPONENT_SWIZZLE_IDENTITY,
				.b = VK_COMPONENT_SWIZZLE_IDENTITY,
				.a = VK_COMPONENT_SWIZZLE_IDENTITY,
			},
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};

		err = vkCreateImageView(device, &info, NULL, &image_views[i]);
		if (err != VK_SUCCESS) {
			SDL_Log("Could not create render pass");
			goto out13;
		}
		num_image_views++;
	}

	uint32_t num_framebuffers = 0;
	VkFramebuffer* framebuffers = malloc(image_count * sizeof(framebuffers[0]));
	for (uint32_t i = 0; i < image_count; i++) {
		VkFramebufferCreateInfo info = {
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.pNext = NULL,
			.flags = 0,
			.renderPass = render_pass,
			.attachmentCount = 3,
			.pAttachments = (VkImageView[]) {
				msaa_view, image_views[i], depth_view
			},
			.width = 1024,
			.height = 768,
			.layers = 1,
		};

		err = vkCreateFramebuffer(device, &info, NULL, &framebuffers[i]);
		if (err != VK_SUCCESS) {
			SDL_Log("Could not create render pass");
			goto out14;
		}
		num_framebuffers++;
	}

	VkShaderModule vertex;
	VkShaderModuleCreateInfo vert_info = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.codeSize = sizeof(VERTEX_SHADER),
		.pCode = (uint32_t*) VERTEX_SHADER,
	};
	err = vkCreateShaderModule(device, &vert_info, NULL, &vertex);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create vertex shader");
		goto out15;
	}

	VkShaderModule pixel;
	VkShaderModuleCreateInfo pixel_info = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.codeSize = sizeof(PIXEL_SHADER),
		.pCode = (uint32_t*) PIXEL_SHADER,
	};
	err = vkCreateShaderModule(device, &pixel_info, NULL, &pixel);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create pixel shader");
		goto out16;
	}

	VkSampler sampler;
	VkSamplerCreateInfo sampler_info = {
		.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.magFilter = VK_FILTER_LINEAR,
		.minFilter = VK_FILTER_LINEAR,
		.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
		.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
		.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
		.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
		.mipLodBias = -1.0f,
		.anisotropyEnable = VK_TRUE,
		.maxAnisotropy = 16.0f,
		.compareEnable = VK_FALSE,
		.minLod = 0.0f,
		.maxLod = 10.0f,
		.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK,
		.unnormalizedCoordinates = VK_FALSE,
	};
	err = vkCreateSampler(device, &sampler_info, NULL, &sampler);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create texture sampler");
		goto out17;
	}

	VkDescriptorSetLayout descriptor_layout;
	VkDescriptorSetLayoutCreateInfo descriptor_layout_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.bindingCount = 2,
		.pBindings = (VkDescriptorSetLayoutBinding[]) {
			[0] = {
				.binding = 0,
				.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
				.pImmutableSamplers = NULL,
			},
			[1] = {
				.binding = 1,
				.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
				.pImmutableSamplers = NULL,
			},
		},
	};
	err = vkCreateDescriptorSetLayout(device, &descriptor_layout_info, NULL, &descriptor_layout);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create descriptor layout");
		goto out17a;
	}

	VkPipelineLayout layout;
	VkPipelineLayoutCreateInfo layout_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.setLayoutCount = 1,
		.pSetLayouts = &descriptor_layout,
		.pushConstantRangeCount = 0,
		.pPushConstantRanges = NULL,
	};
	err = vkCreatePipelineLayout(device, &layout_info, NULL, &layout);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create pipeline layout");
		goto out17b;
	}

	VkPipeline pipeline;
	VkPipelineVertexInputStateCreateInfo vertex_input = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.vertexBindingDescriptionCount = 1,
		.pVertexBindingDescriptions = (VkVertexInputBindingDescription[]) {
			[0] = {
				.binding = 0,
				.stride = 3 * sizeof(float),
				.inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
			},
		},
		.vertexAttributeDescriptionCount = 1,
		.pVertexAttributeDescriptions = (VkVertexInputAttributeDescription[]) {
			[0] = {
				.location = 0,
				.binding = 0,
				.format = VK_FORMAT_R32G32B32_SFLOAT,
				.offset = 0,
			},
		},
	};
	VkPipelineInputAssemblyStateCreateInfo input_assembly = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
		.primitiveRestartEnable = VK_FALSE,
	};
	VkPipelineViewportStateCreateInfo viewport = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.viewportCount = 1,
		.pViewports = NULL, // Dynamic state
		.scissorCount = 1,
		.pScissors = NULL, // Dynamic state
	};
	VkPipelineColorBlendStateCreateInfo color_blend = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.logicOpEnable = VK_FALSE,
		.logicOp = VK_LOGIC_OP_CLEAR,
		.attachmentCount = 1,
		.pAttachments = (VkPipelineColorBlendAttachmentState[]) {
			[0] = {
				.blendEnable = VK_FALSE,
				.colorWriteMask = VK_COLOR_COMPONENT_R_BIT
					| VK_COLOR_COMPONENT_G_BIT
					| VK_COLOR_COMPONENT_B_BIT
					| VK_COLOR_COMPONENT_A_BIT,
			},
		},
		.blendConstants = {0.0f, 0.0f, 0.0f, 0.0f},
	};
	VkPipelineRasterizationStateCreateInfo fragment_stage = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.depthClampEnable = VK_FALSE,
		.rasterizerDiscardEnable = VK_FALSE,
		.polygonMode = VK_POLYGON_MODE_FILL,
		.cullMode = VK_CULL_MODE_BACK_BIT,
		.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
		.depthBiasEnable = VK_FALSE,
		.lineWidth = 1.0f,
	};
	VkPipelineMultisampleStateCreateInfo multisampling = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.rasterizationSamples = VK_SAMPLE_COUNT_8_BIT,
		.sampleShadingEnable = VK_FALSE,
	};
	VkPipelineDepthStencilStateCreateInfo depth_stencil = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.depthTestEnable = VK_TRUE,
		.depthWriteEnable = VK_TRUE,
		.depthCompareOp = VK_COMPARE_OP_LESS,
		.stencilTestEnable = VK_FALSE,
	};
	VkPipelineDynamicStateCreateInfo dynamic_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.dynamicStateCount = 2,
		.pDynamicStates = (VkDynamicState[]) { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR },
	};
	VkGraphicsPipelineCreateInfo pipeline_info = {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.stageCount = 2,
		.pStages = (VkPipelineShaderStageCreateInfo[]) {
			[0] = {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.pNext = NULL,
				.flags = 0,
				.stage = VK_SHADER_STAGE_VERTEX_BIT,
				.module = vertex,
				.pName = "main",
				.pSpecializationInfo = NULL,
			},
			[1] = {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.pNext = NULL,
				.flags = 0,
				.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
				.module = pixel,
				.pName = "main",
				.pSpecializationInfo = NULL,
			},
		},
		.pVertexInputState = &vertex_input,
		.pInputAssemblyState = &input_assembly,
		.pTessellationState = NULL,
		.pViewportState = &viewport,
		.pRasterizationState = &fragment_stage,
		.pMultisampleState = &multisampling,
		.pDepthStencilState = &depth_stencil,
		.pColorBlendState = &color_blend,
		.pDynamicState = &dynamic_state,
		.layout = layout,
		.renderPass = render_pass,
		.subpass = 0,
		.basePipelineHandle = VK_NULL_HANDLE,
		.basePipelineIndex = -1,
	};
	err = vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipeline_info, NULL, &pipeline);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create graphics pipeline");
		goto out18;
	}
	VkBuffer buffer;
	VkBufferCreateInfo buffer_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.size = sizeof(DATA),
		.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
	};
	err = vkCreateBuffer(device, &buffer_info, NULL, &buffer);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create vertex buffer");
		goto out19;
	}

	VkBuffer index_buffer;
	VkBufferCreateInfo index_buffer_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.size = sizeof(INDICES),
		.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
	};
	err = vkCreateBuffer(device, &index_buffer_info, NULL, &index_buffer);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create index buffer");
		goto out19a;
	}

	VkBuffer uniform_buffer;
	VkBufferCreateInfo uniform_buffer_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.size = sizeof(MVP),
		.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
	};
	err = vkCreateBuffer(device, &uniform_buffer_info, NULL, &uniform_buffer);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create index buffer");
		goto out19b;
	}

	VkBuffer staging_buffer;
	VkBufferCreateInfo staging_buffer_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.size = TEXTURE_SIZE,
		.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
	};
	err = vkCreateBuffer(device, &staging_buffer_info, NULL, &staging_buffer);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create index buffer");
		goto out19c;
	}

	VkMemoryRequirements reqs;
	vkGetBufferMemoryRequirements(device, buffer, &reqs);

	VkMemoryRequirements index_reqs;
	vkGetBufferMemoryRequirements(device, index_buffer, &index_reqs);

	VkMemoryRequirements uniform_reqs;
	vkGetBufferMemoryRequirements(device, uniform_buffer, &uniform_reqs);

	VkMemoryRequirements staging_reqs;
	vkGetBufferMemoryRequirements(device, staging_buffer, &staging_reqs);

	VkDeviceSize input_start = 0;
	VkDeviceSize input_end = reqs.size;

	VkDeviceSize uniform_start = (input_end + reqs.alignment - 1) & (-reqs.alignment);
	VkDeviceSize uniform_end = uniform_start + uniform_reqs.size;

	VkDeviceSize index_start = (uniform_end + index_reqs.alignment - 1) & (-index_reqs.alignment);
	VkDeviceSize index_end = index_start + index_reqs.size;

	VkDeviceSize staging_start = (index_end + staging_reqs.alignment - 1) & (-staging_reqs.alignment);
	VkDeviceSize staging_end = staging_start + staging_reqs.size;

	VkDeviceSize buffer_size = staging_end;

	VkDeviceMemory memory;
	VkMemoryAllocateInfo alloc_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.pNext = NULL,
		.allocationSize = buffer_size,
		.memoryTypeIndex = 0,
	};
	err = vkAllocateMemory(device, &alloc_info, NULL, &memory);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create vertex and index buffer memory");
		goto out20;
	}
	vkBindBufferMemory(device, buffer, memory, 0);
	vkBindBufferMemory(device, index_buffer, memory, index_start);
	vkBindBufferMemory(device, uniform_buffer, memory, uniform_start);
	vkBindBufferMemory(device, staging_buffer, memory, staging_start);

	void* data;
	vkMapMemory(device, memory, 0, buffer_size, 0, &data);
	memcpy((char*) data + input_start, DATA, sizeof(DATA));
	memcpy((char*) data + uniform_start, MVP, sizeof(MVP));
	memcpy((char*) data + index_start, INDICES, sizeof(INDICES));
	memcpy((char*) data + staging_start, TEXTURE, TEXTURE_SIZE);
	VkMappedMemoryRange range = {
		.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
		.pNext = NULL,
		.memory = memory,
		.offset = 0,
		.size = buffer_size,
	};
	vkFlushMappedMemoryRanges(device, 1, &range);
	vkUnmapMemory(device, memory);

	VkDescriptorPool descriptor_pool;
	VkDescriptorPoolCreateInfo descriptor_pool_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.maxSets = 1,
		.poolSizeCount = 2,
		.pPoolSizes = (VkDescriptorPoolSize[]) {
			[0] = { .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = 1 },
			[1] = { .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, .descriptorCount = 1 },
		}
	};
	err = vkCreateDescriptorPool(device, &descriptor_pool_info, NULL, &descriptor_pool);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create descriptor pool");
		goto out21;
	}

	VkDescriptorSet descriptor_set;
	VkDescriptorSetAllocateInfo descriptor_set_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
		.pNext = NULL,
		.descriptorPool = descriptor_pool,
		.descriptorSetCount = 1,
		.pSetLayouts = &descriptor_layout,
	};
	err = vkAllocateDescriptorSets(device, &descriptor_set_info, &descriptor_set);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create descriptor pool");
		goto out22;
	}

	VkWriteDescriptorSet descriptor_write = {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.pNext = NULL,
		.dstSet = descriptor_set,
		.dstBinding = 0,
		.dstArrayElement = 0,
		.descriptorCount = 1,
		.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.pImageInfo = NULL,
		.pBufferInfo = (VkDescriptorBufferInfo[]){
			[0] = {.buffer = uniform_buffer, .offset = 0, .range = VK_WHOLE_SIZE},
		},
		.pTexelBufferView = NULL,
	};
	vkUpdateDescriptorSets(device, 1, &descriptor_write, 0, NULL);

	VkWriteDescriptorSet descriptor_write_texture = {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.pNext = NULL,
		.dstSet = descriptor_set,
		.dstBinding = 1,
		.dstArrayElement = 0,
		.descriptorCount = 1,
		.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		.pImageInfo = (VkDescriptorImageInfo[]) {
			[0] = {
				.sampler = sampler, .imageView = texture_view, .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			},
		},
		.pBufferInfo = NULL,
		.pTexelBufferView = NULL,
	};
	vkUpdateDescriptorSets(device, 1, &descriptor_write_texture, 0, NULL);

	VkFence fence;
	VkFenceCreateInfo fence_info = {
		.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
	};
	err = vkCreateFence(device, &fence_info, NULL, &fence);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not create fence");
		goto out22;
	}

	VkQueryPool timestamp_pool;
	VkQueryPoolCreateInfo timestamp_pool_info = {
		.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queryType = VK_QUERY_TYPE_TIMESTAMP,
		.queryCount = 2,
		.pipelineStatistics = 0,
	};
	err = vkCreateQueryPool(device, &timestamp_pool_info, NULL, &timestamp_pool);
	if (err != VK_SUCCESS) {
		SDL_Log("Could not timestamp pool");
		goto out23;
	}

	{
		VkCommandBufferBeginInfo info = {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			.pNext = NULL,
			.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
			.pInheritanceInfo = NULL,
		};
		vkBeginCommandBuffer(cmd_buffers[2], &info);

		VkImageMemoryBarrier transition_barrier = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.pNext = NULL,
			.srcAccessMask = 0,
			.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			.srcQueueFamilyIndex = 0,
			.dstQueueFamilyIndex = 0,
			.image = texture,
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 10,
				.baseArrayLayer = 0,
				.layerCount = 6,
			},
		};
		vkCmdPipelineBarrier(cmd_buffers[2],
							 VK_PIPELINE_STAGE_TRANSFER_BIT,
							 VK_PIPELINE_STAGE_TRANSFER_BIT,
							 0,
							 0, NULL,
							 0, NULL,
							 1, &transition_barrier);

		VkBufferImageCopy regions[6];
		for (int i = 0; i < 6; i++) {
			regions[i] = (VkBufferImageCopy) {
				.bufferOffset = 0,
				.bufferRowLength = 512,
				.bufferImageHeight = 512,
				.imageSubresource = {
					.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
					.mipLevel = 0,
					.baseArrayLayer = i,
					.layerCount = 1,
				},
				.imageOffset = {0, 0, 0},
				.imageExtent = {512, 512, 1},
			};
		}
		vkCmdCopyBufferToImage(cmd_buffers[2], staging_buffer, texture,
							   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
							   6, regions);

		VkImageMemoryBarrier src_barrier = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.pNext = NULL,
			.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
			.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			.srcQueueFamilyIndex = 0,
			.dstQueueFamilyIndex = 0,
			.image = texture,
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 6,
			},
		};
		vkCmdPipelineBarrier(cmd_buffers[2],
							 VK_PIPELINE_STAGE_TRANSFER_BIT,
							 VK_PIPELINE_STAGE_TRANSFER_BIT,
							 0,
							 0, NULL,
							 0, NULL,
							 1, &src_barrier);

		uint32_t mip_width = 512;
		uint32_t mip_height = 512;

		for (uint32_t i = 1; i < 10; i++) {
			VkImageBlit region = {
				.srcSubresource = {
					.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
					.mipLevel = i - 1,
					.baseArrayLayer = 0,
					.layerCount = 6,
				},
				.srcOffsets = {
					[0] = {0, 0, 0},
					[1] = {mip_width, mip_height, 1},
				},
				.dstSubresource = {
					.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
					.mipLevel = i,
					.baseArrayLayer = 0,
					.layerCount = 6,
				},
				.dstOffsets = {
					[0] = {0, 0, 0},
					[1] = {mip_width / 2, mip_height / 2, 1},
				},
			};
			vkCmdBlitImage(cmd_buffers[2],
						   texture, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
						   texture, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
						   1, &region,
						   VK_FILTER_LINEAR);
			src_barrier.subresourceRange.baseMipLevel = i;
			vkCmdPipelineBarrier(cmd_buffers[2],
								 VK_PIPELINE_STAGE_TRANSFER_BIT,
								 VK_PIPELINE_STAGE_TRANSFER_BIT,
								 0,
								 0, NULL,
								 0, NULL,
								 1, &src_barrier);

			mip_width /= 2;
			mip_height /= 2;
		}

		VkImageMemoryBarrier final_barrier = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.pNext = NULL,
			.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
			.dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			.srcQueueFamilyIndex = 0,
			.dstQueueFamilyIndex = 0,
			.image = texture,
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 9,
				.baseArrayLayer = 0,
				.layerCount = 6,
			},
		};
		vkCmdPipelineBarrier(cmd_buffers[2],
							 VK_PIPELINE_STAGE_TRANSFER_BIT,
							 VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
							 0,
							 0, NULL,
							 0, NULL,
							 1, &final_barrier);

		vkEndCommandBuffer(cmd_buffers[2]);

		VkPipelineStageFlags mask = VK_PIPELINE_STAGE_TRANSFER_BIT;
		VkSubmitInfo submit_info = {
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.pNext = NULL,
			.waitSemaphoreCount = 0,
			.pWaitSemaphores = NULL,
			.signalSemaphoreCount = 0,
			.pSignalSemaphores = NULL,
			.pWaitDstStageMask = &mask,
			.commandBufferCount = 1,
			.pCommandBuffers = &cmd_buffers[2],
		};
		vkQueueSubmit(queue, 1, &submit_info, fence);
		vkWaitForFences(device, 1, &fence, VK_TRUE, UINT64_MAX);
		vkResetFences(device, 1, &fence);
	}

	{
		for (size_t i = 0; i < image_count; i++) {
			VkCommandBufferBeginInfo info = {
				.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
				.pNext = NULL,
				.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
				.pInheritanceInfo = NULL,
			};
			vkBeginCommandBuffer(cmd_buffers[i], &info);

			vkCmdResetQueryPool(cmd_buffers[i], timestamp_pool, 0, 2);
			vkCmdWriteTimestamp(cmd_buffers[i], VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, timestamp_pool, 0);

			VkRenderPassBeginInfo begin_info = {
				.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
				.pNext = NULL,
				.renderPass = render_pass,
				.framebuffer = framebuffers[i],
				.renderArea = {
					.offset = {0, 0},
					.extent = {1024, 768},
				},
				.clearValueCount = 3,
				.pClearValues = (VkClearValue[]) {
					[0] = {
						.color.float32 = { 0.1f, 0.2f, 0.4f, 1.0f },
					},
					[1] = {
						.color.float32 = { 0.0f, 0.0f, 0.0f, 1.0f },
					},
					[2] = {
						.depthStencil = { 1.0f, 0 },
					},
				},
			};

			vkCmdBeginRenderPass(cmd_buffers[i], &begin_info, VK_SUBPASS_CONTENTS_INLINE);
			vkCmdBindPipeline(cmd_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

			VkViewport viewport = {
				.x = 0,
				.y = 0,
				.width = 1024,
				.height = 768,
				.minDepth = 0.0f,
				.maxDepth = 1.0f,
			};
			VkRect2D scissor = {
				.offset = {0, 0},
				.extent = {1024, 768},
			};

			vkCmdSetViewport(cmd_buffers[i], 0, 1, &viewport);
			vkCmdSetScissor(cmd_buffers[i], 0, 1, &scissor);

			VkDeviceSize offsets[] = {0};
			vkCmdBindDescriptorSets(cmd_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, layout, 0, 1, &descriptor_set, 0, NULL);
			vkCmdBindVertexBuffers(cmd_buffers[i], 0, 1, &buffer, offsets);
			vkCmdBindIndexBuffer(cmd_buffers[i], index_buffer, 0, VK_INDEX_TYPE_UINT16);

			vkCmdDrawIndexed(cmd_buffers[i], 36, 1, 0, 0, 0);

			vkCmdEndRenderPass(cmd_buffers[i]);

			vkCmdWriteTimestamp(cmd_buffers[i], VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, timestamp_pool, 1);

			vkEndCommandBuffer(cmd_buffers[i]);
		}
	}

	SDL_Event event;
	for (;;) {
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				goto exit_event_loop;
			}
		}

		uint32_t idx;
		vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, semaphore_img_available, VK_NULL_HANDLE, &idx);

		VkPipelineStageFlags mask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		VkSubmitInfo info = {
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.pNext = NULL,
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &semaphore_img_available,
			.signalSemaphoreCount = 1,
			.pSignalSemaphores = &semaphore_rendering_done,
			.pWaitDstStageMask = &mask,
			.commandBufferCount = 1,
			.pCommandBuffers = &cmd_buffers[idx],
		};

		vkQueueSubmit(queue, 1, &info, fence);

		VkPresentInfoKHR present_info = {
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.pNext = NULL,
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &semaphore_rendering_done,
			.swapchainCount = 1,
			.pSwapchains = &swapchain,
			.pImageIndices = &idx,
			.pResults = NULL,
		};

		vkQueuePresentKHR(queue, &present_info);
		vkWaitForFences(device, 1, &fence, VK_TRUE, UINT64_MAX);

		uint64_t timestamps[2];
		vkGetQueryPoolResults(device, timestamp_pool, 0, 2,
							  sizeof(timestamps), timestamps,
							  sizeof(timestamps[0]),
							  VK_QUERY_RESULT_64_BIT | VK_QUERY_RESULT_WAIT_BIT);
		printf("Time taken to draw frame: %f msec\n", (timestamps[1] - timestamps[0]) * device_limits.limits.timestampPeriod / 1e6);

		vkResetFences(device, 1, &fence);
	}
exit_event_loop:
	vkDeviceWaitIdle(device);

	retval = EXIT_SUCCESS;

	vkDestroyQueryPool(device, timestamp_pool, NULL);
out23:
	vkDestroyFence(device, fence, NULL);
out22:
	vkDestroyDescriptorPool(device, descriptor_pool, NULL);
out21:
	vkFreeMemory(device, memory, NULL);
out20:
	vkDestroyBuffer(device, staging_buffer, NULL);
out19c:
	vkDestroyBuffer(device, index_buffer, NULL);
out19b:
	vkDestroyBuffer(device, uniform_buffer, NULL);
out19a:
	vkDestroyBuffer(device, buffer, NULL);
out19:
	vkDestroyPipeline(device, pipeline, NULL);
out18:
	vkDestroyPipelineLayout(device, layout, NULL);
out17b:
	vkDestroyDescriptorSetLayout(device, descriptor_layout, NULL);
out17a:
	vkDestroySampler(device, sampler, NULL);
out17:
	vkDestroyShaderModule(device, pixel, NULL);
out16:
	vkDestroyShaderModule(device, vertex, NULL);
out15:
	for (uint32_t i = 0; i < num_framebuffers; i++) {
		vkDestroyFramebuffer(device, framebuffers[i], NULL);
	}
	free(framebuffers);
out14:
	for (uint32_t i = 0; i < num_image_views; i++) {
		vkDestroyImageView(device, image_views[i], NULL);
	}
	free(image_views);
out13:
	vkDestroyImageView(device, texture_view, NULL);
out12g:
	vkDestroyImageView(device, depth_view, NULL);
out12f:
	vkDestroyImageView(device, msaa_view, NULL);
out12e:
	vkFreeMemory(device, msaa_mem, NULL);
out12d:
	vkDestroyImage(device, texture, NULL);
out12c:
	vkDestroyImage(device, depth_buffer, NULL);
out12b:
	vkDestroyImage(device, msaa_buffer, NULL);
out12a:
	vkDestroyRenderPass(device, render_pass, NULL);
out12:
	vkDestroySemaphore(device, semaphore_rendering_done, NULL);
out11:
	vkDestroySemaphore(device, semaphore_img_available, NULL);
out10:
	vkDestroyCommandPool(device, cmd_pool, NULL);
out9:
	free(images);
out8:
	vkDestroySwapchainKHR(device, swapchain, NULL);
out7:
	vkDestroySurfaceKHR(instance, surface, NULL);
out6:
	SDL_DestroyWindow(window);
out5:
	vkDestroyDevice(device, NULL);
out4:
	free(devices);
out3:
	vkDestroyInstance(instance, NULL);
out2:
	SDL_Quit();
out1:
	return retval;
}
